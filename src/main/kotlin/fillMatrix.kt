class Builder(private val matrix: Matrix) {

    private class Coord(val y: Int, val x: Int) {
        fun move(direction: Coord): Coord {
            return Coord(y + direction.y, x + direction.x)
        }
    }

    private class Element(val c: Coord, val value: Int)

    private val UP = Coord(-1, 0)
    private val UP_LEFT = Coord(-1, -1)
    private val DOWN = Coord(1, 0)
    private val DOWN_RIGHT = Coord(1, 1)
    private val RIGHT = Coord(0, 1)
    private val STOP = Coord(0, 0)

    /**
     * Первый проход. Под главной диагональю
     */
    private fun goOne(start: Element): Element {
        fun checkMainDiagonal(coord: Coord): Boolean {
            return coord.x == coord.y
        }

        var direction = DOWN
        var c = start.c
        var next: Coord
        var v = start.value
        while (direction != STOP) {
            matrix.setElem(c.y, c.x, v)
            next = c.move(direction)
            if (!matrix.inBounds(next.y, next.x)) { // уперлись в нижний край
                direction = RIGHT
            } else
            if (checkMainDiagonal(next)) {
                direction =
                if (direction == UP) { // уперлись в диагональ снизу
                    DOWN_RIGHT
                } else { // уперлись в диагональ слева
                    STOP
                }
            }
            else {
                c = next
                v++
                if (direction == RIGHT) {
                    direction = UP
                }
                if (direction == DOWN_RIGHT) {
                    direction = DOWN
                }
            }
        }
        return Element(c, v)
    }


    private fun goTwo(start: Element) {
        var direction = UP_LEFT
        var c = start.c
        var next: Coord
        var v = start.value
        while (direction != STOP) {
            matrix.setElem(c.y, c.x, v)
            next = c.move(direction)
            if (!matrix.inBounds(next.y, next.x)) { // уперлись в край
                direction =
                if (direction == RIGHT || direction == UP)
                    STOP
                else {
                    if (direction == UP_LEFT) {
                        RIGHT
                    } else { // direction == DOWN_RIGHT
                        UP
                    }
                }
            }
            else {
                c = next
                v++
                if (direction == RIGHT) {
                    direction = DOWN_RIGHT
                }
                if (direction == UP) {
                    direction = UP_LEFT
                }
            }
        }
    }

    /**
     * Заполнение матрицы
     */
    fun build() {
        if (matrix.size > 1) {
            val cur = goOne(Element(Coord(1, 0), 1))
            goTwo(Element(cur.c.move(RIGHT), cur.value + 1))
        }
        else {
            matrix.setElem(0, 0, 1)
        }
    }

}

/**
 * Заполнение матрицы
 */
fun fillMatrix(matrix: Matrix) {
    val builder = Builder(matrix)
    builder.build()
}
