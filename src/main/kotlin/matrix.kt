/**
 * Квадратная матрица
 * @param size размерность
 */
class Matrix(val size: Int) {
    init {
        if (size <= 0 )
            throw IllegalArgumentException("size cant be $size")
    }

    private val elements = Array<IntArray>(size) { IntArray(size) }

    /**
     * Проверка на наличие элемента в матрице
     * @param y строка
     * @param x столбец
     * @return присутствует ли элемент
     */
    fun inBounds(y: Int, x: Int): Boolean {
        return x in 0 until size && y in 0 until size
    }

    /**
     * Получение элемента матрицы
     * @param y строка
     * @param x столбец
     * @return значение элемента
     */
    fun getElem(y: Int, x: Int): Int {
        if (!inBounds(y, x))
            throw IllegalArgumentException("element [$y, $x] does not exists")
        return elements[x][y]
    }

    /**
     * Присваивание элементу матрицы значения
     * @param y строка
     * @param x столбец
     * @param value значение элемента
     */
    fun setElem(y: Int, x: Int, value: Int) {
        if (!inBounds(y, x))
            throw IllegalArgumentException("element [$y, $x] does not exists")
        elements[x][y] = value
    }
}
