import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class FillMatrix {

    @Test
    fun fillMatrix1x1() {
        val m = Matrix(1)
        fillMatrix(m)
        assertEquals(1, m.getElem(0, 0))
    }

    @Test
    fun fillMatrix2x2() {
        val m = Matrix(2)
        fillMatrix(m)
        assertEquals(3, m.getElem(0, 0))
        assertEquals(4, m.getElem(0, 1))
        assertEquals(1, m.getElem(1, 0))
        assertEquals(2, m.getElem(1, 1))
    }

    @Test
    fun fillMatrix3x3() {
        val m = Matrix(3)
        fillMatrix(m)
        assertEquals(6, m.getElem(0, 0))
        assertEquals(7, m.getElem(0, 1))
        assertEquals(9, m.getElem(0, 2))
        assertEquals(1, m.getElem(1, 0))
        assertEquals(5, m.getElem(1, 1))
        assertEquals(8, m.getElem(1, 2))
        assertEquals(2, m.getElem(2, 0))
        assertEquals(3, m.getElem(2, 1))
        assertEquals(4, m.getElem(2, 2))
    }
}