import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class MatrixTest {

    @Test
    fun getSetElem() {
        val m = Matrix(3)
        m.setElem(1, 1, 666)
        assertEquals(m.getElem(1, 1), 666)
        m.setElem(1, 1, 6)
        assertNotEquals(m.getElem(1, 1), 666)
    }

    @Test
    fun getSize() {
        val size = 5
        val m = Matrix(size)
        assertEquals(m.size, size)
    }

    @Test
    fun negativeSize() {
        assertThrows(IllegalArgumentException::class.java)  {
            val m = Matrix(-1)
        }
    }
}